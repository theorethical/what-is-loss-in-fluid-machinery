# what is loss in fluid machinery

M.TANAKA  
作成：2023/04/19  
更新：2023/06/09  

[TOC]


# 1. この文書について

自分の考えを整理するためのメモである．

流体機械の効率を考える上で，損失を定義する必要がある．全エネルギーは保存されるのだから，損失とはエネルギーの消失ではなく，工学的に利用できない形式のエネルギーへの変換を指していると思われる．

そうすると，「工学的に利用できるエネルギー」とは何かが問題になるが，これが良く分からない（特に，圧縮性流体において）．そのあたりについて考える．

## 1.1 前提

- 流れ場は定常である．
  - 一般性を考慮して，式展開は可能な限り非定常でも成り立つように変形するよう心掛ける．定常の仮定を用いる場合は文中で明示する．
- 系は断熱であり，境界からの熱伝導や外部からの輻射伝熱は起こらない．
  - 一般性を考慮して同上．
- 流れに働く体積力（例えば重力など）は無視できる．

## 1.2 よく使う関係式

式展開で何度も使う式について，先に証明なしで列挙しておく．

### 1.2.1 数学的な式

#### ガウスの定理

$$
\begin{align}
  \int_V \nabla \cdot \mathcal{F} dV &= \int_{\partial V} ( d \bm{S} \cdot \mathcal{F} )
\end{align}
$$

| 記号          | 説明      |
|--------------|-----------|
| $V$          | 任意の体積 |
| $\partial V$ | $V$ の境界 |
| $d \bm{S}$   | 面積要素ベクトル．大きさは面積に等しく，向きは $V$ の外向き法線方向に一致する． |
| $\mathcal{F}$ | 任意の空間分布量．ベクトルまたはテンソル． |

- 補足: スカラーについても同様の関係式が成り立つ．それには $\mathcal{F} = \mathcal{f} \bm{I}$ とすればよい．

$$
\begin{align}
  \int_V \nabla \cdot (\mathcal{f} \bm{I}) dV &= \int_{\partial V} [ d \bm{S} \cdot (\mathcal{f} \bm{I}) ]
  \\
  \therefore
  \int_V \nabla \mathcal{f} dV &= \int_{\partial V} \mathcal{f} d \bm{S} 
\end{align}
$$

### 1.2.2 物理的な式

#### 連続の式

- 全て等価だが，特に下２通りの表現がよく使われる．

$$
\begin{align}
  \frac{\partial \rho}{\partial t} 
    + \rho (\nabla \cdot \bm{u}) 
    + \bm{u} \cdot \nabla \rho &= 0 
  \\
  \frac{D \rho}{D t} 
    + \rho (\nabla \cdot \bm{u}) &= 0 
  \\
  \frac{\partial \rho}{\partial t} 
    + \nabla \cdot (\rho \bm{u}) &= 0
\end{align}
$$

# 2. 流体におけるエネルギー保存

## 2.1 まとめ

ある検査体積 $V$ の内部の流体のエネルギー収支の式は次のようになる．

$$
  \int_{V} \rho \frac{DE}{Dt} dV 
  = 
  \int_{\partial V} 
  (-p \bm{n} 
    + \bm{n} \cdot 
    \{ 
      \lambda (\nabla \cdot \bm{u}) \bm{I}
      + \mu [ \nabla \bm{u} + (\nabla \bm{u})^T ] 
    \} 
  ) \cdot \bm{u} 
  dS
  +
  \int_{\partial V} \bm{n} \cdot k \nabla T dS.
$$

| 記号と単位                            | 意味             |
|:-------------------------------------|:----------------|
| $\rho \quad / \quad \mathrm{kg/m^3}$ | 流体の密度．       |
| $E \quad / \quad \mathrm{J/kg}$      | 単位質量当たりの全エネルギー．内部エネルギー $e$ と運動エネルギー $u^2 /2$ の和． |
| $p \quad / \quad \mathrm{Pa}$               | 流体の圧力．      |
| $dS \quad / \quad \mathrm{m^2}$       | 体積表面の微小面積要素． |
| $\bm{n} \quad / \quad -$              | 面 $dS$ の外向き法線ベクトル．後に $d\bm{S} = \bm{n} dS$ とも表記． |
| $\lambda \quad / \quad \mathrm{Pa \cdot s}$ | 第二粘性係数．    |
| $\bm{u} \quad / \quad \mathrm{m/s}$  | 流体の速度ベクトル． |
| $\mu \quad / \quad \mathrm{Pa \cdot s}$     | 粘性係数．        |
| $k \quad / \quad \mathrm{W/(m \cdot K)}$ | 流体の熱伝導率(heat/thermal conductiviy) |
| $T \quad / \quad \mathrm{K}$ | 流体の温度．       |


[詳細は別ファイルにジャンプ](./S2.md)


# 3. 流体機械におけるエネルギー保存

## 3.1 まとめ

系のエネルギー収支の式は次のようになる．

$$
  P
  =
  \int_{S_{let}} 
    d \bm{S} \cdot ( \rho \bm{u} H ) 
  +
  \int_{S_{imp}} 
    d \bm{S} \cdot ( \rho \bm{u} E ) 
  +
  \int_{V} 
    \frac{\partial}{\partial t} (\rho E) 
  dV .
$$

特に，流れが定常であれば

$$
  P
  =
  \int_{S_{let}} 
    d \bm{S} \cdot ( \rho \bm{u} H ) 
  +
  \int_{S_{imp}} 
    d \bm{S} \cdot ( \rho \bm{u} E ) .
$$

| 記号と単位                            | 意味             |
|:-------------------------------------|:----------------|
| $P \quad / \quad \mathrm{W}$         | 流体機械の軸動力． |
| $H \quad / \quad \mathrm{J/kg}$      | 単位質量当たりの全エンタルピー． $H=E+p/\rho = e + u^2/2 + p/\rho$ |

[詳細は別ファイルにジャンプ](./S3.md)


# 4. 流れの損失とは何か

## 4.1 まとめ

エネルギーの散逸＝熱とすれば，単位時間に散逸するエネルギー $P_{loss}$ は以下で定義できる．

$$
  P_{loss} 
  \equiv 
  \int_{V} \left(
    \rho T \frac{Ds}{Dt} 
    +
    \nabla \cdot \bm{q}
  \right) dV  .
$$

| 名前と単位                                  | 意味      |
|:------------------------------------------|:---------|
| $T \quad / \quad \mathrm{K}$              | 流体の温度 |
| $s \quad / \quad \mathrm{J/(kg \cdot K)}$ | 流体の比エントロピー |

[詳細は別ファイルにジャンプ](./S4.md)


# 5. 損失と流れの量との関係

## 5.1 まとめ

エントロピーを用いて定義した流れの損失について，以下の関係が成り立つ．

$$
  P_{loss} 
=
  \int_{\partial V}
    d\bm{S} \cdot \left(
      \rho \bm{u} e + \bm{q}
    \right)
  +
  \int_V \left[
    \frac{\partial}{\partial t}(\rho e) + p (\nabla \cdot \bm{u})
  \right] dV 
$$

特に，流れが定常かつ非圧縮性とみなせるなら，

$$
  P_{loss} 
=
  \int_{\partial V}
    d\bm{S} \cdot \left(
      \rho \bm{u} e + \bm{q}
    \right)
$$

| 記号と単位                                  | 意味        |
|:------------------------------------------|:-----------|
| $e \quad / \quad \mathrm{J/kg}$           | 流体の単位質量当たりの内部エネルギー． |

[詳細は別ファイルにジャンプ](./S5.md)


# 6. 流体機械の効率の式の導出

## 6.1 まとめ

$$
  \eta 
  = 
  \frac
    {
      \int_{S_{let}} 
        d \bm{S} \cdot \left[ 
          \rho \bm{u} \left( \frac{u^2}{2} + \frac{p}{\rho} \right)
        \right]
      +
      \int_{S_{imp}} 
        d \bm{S} \cdot \left( 
          \rho \bm{u} \frac{u^2}{2}
        \right)
      -
      \int_{\partial V}
        d\bm{S} \cdot \bm{q}
      +
      \int_{V} 
        \frac{\partial}{\partial t} (\rho \frac{u^2}{2}) 
      dV
      -
      \int_V
        p (\nabla \cdot \bm{u})
      dV
    }
    {
      \int_{S_{let}} 
        d \bm{S} \cdot ( \rho \bm{u} H ) 
      +
      \int_{S_{imp}} 
          d \bm{S} \cdot ( \rho \bm{u} E ) 
      +
      \int_{V} 
        \frac{\partial}{\partial t} (\rho E) 
      dV
    } .
$$

とくに，系が断熱で，流れが定常かつ非圧縮性とみなせるなら，

$$
  \eta 
  = 
  \frac
    {
      \int_{S_{let}} 
        d \bm{S} \cdot ( p_t \bm{u} )
      +
      \int_{S_{imp}} 
        d \bm{S} \cdot ( p_d \bm{u} )
    }
    {
      \int_{S_{let}} 
        d \bm{S} \cdot ( \rho \bm{u} H ) 
      +
      \int_{S_{imp}} 
          d \bm{S} \cdot ( \rho \bm{u} E ) 
    } .
$$

さらに，

- インペラが薄板より成る
- 入口出口のそれぞれで全圧が一様である
- 軸受やモータ内の損失を無視できる

とすれば，[全圧効率](http://www.mekatoro.net/digianaecatalog/showa-t1t2/Book/showa-t1t2-P0081.pdf)の式が得られる．

$$
  \eta 
  = 
  \frac
    {
      Q \Delta p_t
    }
    {
      P_{motor}
    }
$$

| 名前と単位                               | 意味      |
|:---------------------------------------|:---------|
| $p_t \quad / \quad \mathrm{Pa}$        | 流体の全圧 |
| $p_d \quad / \quad \mathrm{Pa}$        | 流体の動圧 |
| $Q \quad / \quad \mathrm{m^3/s}$       | 流体機械の体積流量 |
| $\Delta p_t \quad / \quad \mathrm{Pa}$ | 入口出口間の全圧差 |
| $P_{motor} \quad / \quad \mathrm{W}$   | モータの消費電力 |

[詳細は別ファイルにジャンプ](./S6.md)


# 7. 損失の式の具体的な姿

## 7.1 まとめ

$$
  P_{loss} 
  =       \int_{V} \bm{V} : \bm{D} dV 
  =       \int_{V} \left\{
            \lambda (\nabla \cdot \bm{u})^2 
          + \frac{1}{2} \mu [ \nabla \bm{u} + (\nabla \bm{u})^T ] : [ \nabla \bm{u} + (\nabla \bm{u})^T ] 
          \right\} dV .
$$

$$
  \bm{V} \equiv \lambda (\nabla \cdot \bm{u}) \bm{I} + 2 \mu \bm{D} .
$$

$$
  \bm{D} \equiv \frac{1}{2} [ \nabla \bm{u} + (\nabla \bm{u})^T ] .
$$

| 記号と単位                          | 意味        |
|:-----------------------------------|:-----------|
| $\bm{V} \quad / \quad \mathrm{Pa}$ | 流体の粘性応力テンソル(viscous stress tensor)． |
| $\bm{D} \quad / \quad \mathrm{1/s}$ | 流体のひずみ速度テンソル(strain/deformation rate tensor)． |

[詳細は別ファイルにジャンプ](./S7.md)


# 8. 理論解への損失の式の適用

[詳細は別ファイルにジャンプ](./S8.md)


# 参考

webサイトはいずれも2023年04月26日に最終アクセス．

## 式展開一般
- Wu, J. Z., Ma, H. Y., & Zhou, M. D. (2015). *Vortical Flows.* New York, NY: Springer.

- [Notes on Computational Fluid Dynamics: General Principles. Chapter 2 Fluid Dynamics](https://doc.cfd.direct/notes/cfd-general-principles/fluid-dynamics)

## 運動方程式：
- [Notes on Computational Fluid Dynamics: General Principles. Chapter 2 Fluid Dynamics 2.7 Conservation of momentum](https://doc.cfd.direct/notes/cfd-general-principles/conservation-of-momentum#x21-290007)

## 全エネルギーの微分：
- [Notes on Computational Fluid Dynamics: General Principles. Chapter 2 Fluid Dynamics 2.15 Conservation of energy](https://doc.cfd.direct/notes/cfd-general-principles/conservation-of-energy#x38-4600015)

## テンソル演算：
- [Notes on Computational Fluid Dynamics: General Principles. Chapter 2 Fluid Dynamics 2.24 Summary of tensor algebra](https://doc.cfd.direct/notes/cfd-general-principles/summary-of-tensor-algebra)

## ベクトル恒等式：
- [Notes on Computational Fluid Dynamics: General Principles. Chapter 2 Fluid Dynamics 2.25 Vector identities](https://doc.cfd.direct/notes/cfd-general-principles/vector-identities)
