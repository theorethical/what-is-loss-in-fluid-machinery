[本文に戻る](./README.md#2-流体におけるエネルギー保存)

[TOC]

# 2. 流体におけるエネルギー保存

## 2.1 まとめ
結論だけ先取りすると，ある検査体積 $V$ の内部の流体のエネルギー収支の式は次のようになる．

$$
  \int_{V} \rho \frac{DE}{Dt} dV 
  = 
  \int_{\partial V} 
  (-p \bm{n} 
    + \bm{n} \cdot 
    \{ 
      \lambda (\nabla \cdot \bm{u}) \bm{I}
      + \mu [ \nabla \bm{u} + (\nabla \bm{u})^T ] 
    \} 
  ) \cdot \bm{u} 
  dS
  +
  \int_{\partial V} \bm{n} \cdot k \nabla T dS.
$$

| 記号と単位                            | 意味             |
|:-------------------------------------|:----------------|
| $\rho \quad / \quad \mathrm{kg/m^3}$ | 流体の密度．       |
| $E \quad / \quad \mathrm{J/kg}$      | 単位質量当たりの全エネルギー．内部エネルギー $e$ と運動エネルギー $u^2 /2$ の和． |
| $p \quad / \quad \mathrm{Pa}$               | 流体の圧力．      |
| $dS \quad / \quad \mathrm{m^2}$       | 体積表面の微小面積要素． |
| $\bm{n} \quad / \quad -$              | 面 $dS$ の外向き法線ベクトル．後に $d\bm{S} = \bm{n} dS$ とも表記． |
| $\lambda \quad / \quad \mathrm{Pa \cdot s}$ | 第二粘性係数．    |
| $\bm{u} \quad / \quad \mathrm{m/s}$  | 流体の速度ベクトル． |
| $\mu \quad / \quad \mathrm{Pa \cdot s}$     | 粘性係数．        |
| $k \quad / \quad \mathrm{W/(m \cdot K)}$ | 流体の熱伝導率(heat/thermal conductiviy) |
| $T \quad / \quad \mathrm{K}$ | 流体の温度．       |


## 2.2 物質体積と検査体積

まず，エネルギーの釣り合い式を出発点にする．そのために，物質体積と検査体積の説明から始める．

ある時刻(便宜的に $t=0$ とする)における流れの中に，閉曲面で囲われた領域 $\mathcal{V}$ を考える．領域 $\mathcal{V}$ は，それを構成する流体粒子に追従して移動するとする．このような体積領域を物質体積(material volume)と呼ぶ．

また， $t=0$ においては $\mathcal{V}$ と同一で，それ以降も空間内の同じ位置に固定され続けるような体積領域 $V$ を検査体積(control volume)と呼ぶ．

```
             dV
    *------------------*    
    |     .............|......
    | udt :            |     :
    |---->:   V        | udt :
    |     :            |---->:
    *------------------*     :
          :..................:

--- : 検査体積(control volume)．空間に固定されている．
... : 物質体積(material volume)．流体粒子に追従して移動する．    
```
## 2.3 エネルギーの釣り合い式（一般的な形）

ある物質体積 ${\mathcal{V}}$ に関するエネルギーの釣り合いは，以下で表現される．

$$
\begin{align}
  \frac{d}{dt} \int_{\mathcal{V}} \rho E d \mathcal{V} 
  &= 
  \int_{\partial \mathcal{V}} \bm{t} \cdot \bm{u} dS
  +
  \int_{\partial \mathcal{V}} (-\bm{n}) \cdot \bm{q} dS .
\end{align}
$$

この式は，物質体積が持つ全エネルギーの変化(結果)は，体積の表面を通じて与えられる仕事や熱(原因)に等しいことを意味している．各記号の意味は以下の通り．

| 記号と単位                             | 意味        |
|:--------------------------------------|:-----------|
| $\rho \quad / \quad \mathrm{kg/m^3}$  | 流体の密度． |
| $E \quad / \quad \mathrm{J/kg}$       | 単位質量当たりの全エネルギー．内部エネルギー $e$ と運動エネルギー $u^2 /2$ の和． |
| $\bm{u} \quad / \quad \mathrm{m/s}$   | 流体の速度ベクトル． |
| $dS \quad / \quad \mathrm{m^2}$       | 体積表面の微小面積要素． |
| $\bm{t} \quad / \quad \mathrm{Pa}$    | 面 $dS$ に作用する流体の応力ベクトル． $\bm{t} = \bm{n} \cdot \bm{T}$ と表される． |
| $\bm{n} \quad / \quad -$              | 面 $dS$ の外向き法線ベクトル．後に $d\bm{S} = \bm{n} dS$ とも表記． |
| $\bm{T} \quad / \quad \mathrm{Pa}$    | 流体の応力テンソル． |
| $\bm{q} \quad / \quad \mathrm{W/m^2}$ | 熱流束ベクトル． |

ここで，[導出は後述する](#appendix)が，任意のスカラー場 $F$ について以下の等式が成り立つ．

$$
  \frac{d}{dt} \int_{\mathcal{V}} \rho F d \mathcal{V} 
  =
  \int_{V} \rho \frac{DF}{Dt} dV .  
$$

これを使って先の式を整えると，

$$
\begin{align}
  \int_{V} \rho \frac{DE}{Dt} dV 
  &= 
  \int_{\partial V} d\bm{S} \cdot \bm{T} \cdot \bm{u}
  -
  \int_{\partial V} d\bm{S} \cdot \bm{q} .
\end{align}
$$

を得る．

### 2.3.1 おまけ

式(2)に対しガウスの定理を用いると，面積分を体積分に変換できて，

$$
\begin{align}
  \int_{V} \rho \frac{DE}{Dt} dV 
  &= 
  \int_{V} \nabla \cdot (\bm{T} \cdot \bm{u}) dV
  -
  \int_{V} \nabla \cdot \bm{q} dV.
\end{align}
$$

検査領域は任意に選べるから，局所的な釣り合いの式

$$
\begin{align}
  \rho \frac{DE}{Dt}
  &= 
  \nabla \cdot (\bm{T} \cdot \bm{u})
  -
  \nabla \cdot \bm{q}.
\end{align}
$$

を得る．

## 2.4 構成則

応力テンソル $\bm{T}$ と熱流束 $\bm{q}$ は，仮定を置くことによって流体の基本的な状態量の関数として表すことができる．

### 2.4.1 ニュートンの粘性則

ニュートンの粘性則を仮定すると，応力テンソル $\bm{T}$ は圧力 $p$ と速度 $\bm{u}$ の関数として次のように表すことができる．

$$
\begin{align}
  \bm{T} &= -p \bm{I} + \bm{V} .
  \\
  \bm{V} 
  &= 
  \lambda (\nabla \cdot \bm{u}) \bm{I}
  +
  \mu [ \nabla \bm{u} + (\nabla \bm{u})^T ]
\end{align}
$$


| 記号と単位                                    | 意味            |
|:--------------------------------------------|:----------------|
| $p \quad / \quad \mathrm{Pa}$               | 流体の圧力．      |
| $\bm{V} \quad / \quad \mathrm{Pa}$          | 粘性応力テンソル． |
| $\mu \quad / \quad \mathrm{Pa \cdot s}$     | 粘性係数．        |
| $\lambda \quad / \quad \mathrm{Pa \cdot s}$ | 第二粘性係数．    |

したがって，

$$
\begin{align}
  \bm{t} &= \bm{n} \cdot \bm{T} 
  = -p \bm{n} + \bm{n} \cdot \bm{V} .
\end{align}
$$

である．

### 2.4.2 フーリエ則

フーリエ則を仮定すると，熱流束 $\bm{q}$ は温度 $T$ の関数として次のように表すことができる．

$$
\begin{align}
  \bm{q} = -k \nabla T
\end{align}
$$

| 記号と単位                                 | 意味 |
|:-----------------------------------------|:-----|
| $k \quad / \quad \mathrm{W/(m \cdot K)}$ | 流体の熱伝導率(heat/thermal conductivity)． |

### 2.4.3 上記２つの仮説を代入

エネルギーの釣り合い式(2)は次のようになる．

$$
\begin{align}
  \int_{V} \rho \frac{DE}{Dt} dV 
  &= 
  \int_{\partial V} 
  (-p \bm{n} 
    + \bm{n} \cdot 
    \{ 
      \lambda (\nabla \cdot \bm{u}) \bm{I}
      + \mu [ \nabla \bm{u} + (\nabla \bm{u})^T ] 
    \} 
  ) \cdot \bm{u} 
  dS
  +
  \int_{\partial V} \bm{n} \cdot k \nabla T dS.
\end{align}
$$



## Appendix

まず，必要な式について証明なしに示す．

- 連続の式

$$
\begin{align}
  \frac{\partial \rho}{\partial t} 
  + \bm{u} \cdot \nabla \rho
  + \rho (\nabla \cdot \bm{u}) 
  = 
  0.
\end{align}
$$

- Reynoldsの輸送定理

$$
\begin{align}
  \frac{d}{dt} \int_{\mathcal{V}} F 
  d \mathcal{V} 
  &= 
  \int_{V} \frac{\partial F}{\partial t} dV 
  + 
  \int_{\partial V} d \bm{S} \cdot (\bm{u} F ) \\
  &= 
  \int_{V} \left[ 
    \frac{\partial F}{\partial t}
    + 
    \nabla \cdot (\bm{u} F ) 
  \right] dV.
\end{align}
$$

被積分スカラー場関数が $\rho F$ の形をとるとき，物質体積分に対する時間微分は，レイノルズの輸送定理と連続の式により，物質微分の検査体積分に置き換えることができる．

$$
\begin{align}
  \frac{d}{dt} \int_{\mathcal{V}} \rho F 
  d \mathcal{V} 
  &= 
  \int_{V} \left[ \frac{\partial}{\partial t} (\rho F) 
  + \nabla  \cdot (\bm{u} \rho F) \right] dV \\
  &= 
  \int_{V} \left[ F \frac{\partial \rho}{\partial t} 
  + \rho \frac{\partial F}{\partial t} 
  + \rho F (\nabla \cdot \bm{u}) 
  + F \bm{u} \cdot \nabla \rho 
  + \rho \bm{u} \cdot \nabla F \right] dV \\
  &= 
  \int_{V} F \left[ \frac{\partial \rho}{\partial t} 
  + \rho (\nabla \cdot \bm{u}) 
  + \bm{u} \cdot \nabla \rho \right] dV 
  + 
  \int_{V} \rho \left[ \frac{\partial F}{\partial t} 
  + \bm{u} \cdot \nabla F \right] dV \\
  &= 
  \int_{V} \rho \frac{DF}{Dt} dV \\
\end{align}
$$


[次へ](./S3.md)
